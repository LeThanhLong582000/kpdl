# Requirements
* Python3 >= 3.7
* tensorflow = 2.8.0
* pandas = 1.3.5


# Dataset

## Layout

```bash
data-folder
├── astilbe
│   ├── img1.jpg
│   ├── img2.jpg
│   ├── ...
├── bellflower
│   ├── img1.jpg
│   ├── img2.jpg
│   ├── ...
├── black-eyed-susan
│   ├── img1.jpg
│   ├── img2.jpg
│   ├── ...
├── calendula
│   ├── img1.jpg
│   ├── img2.jpg
│   ├── ...
├── california-poppy
│   ├── img1.jpg
│   ├── img2.jpg
│   ├── ...
├── tulip
│   ├── img1.jpg
│   ├── img2.jpg
│   ├── ...
```


# Training

You can run simple training by just running `./train.sh` or you can custom your training by using a training script.

## Training Script

```bash
python3 train.py \
--train_directory PATH_TO_FOLDER_STORE_TRAINING_DATA \
--val_directory PATH_TO_FOLDER_STORE_VALIDATION_DATA \
--model_directory PATH_TO_FOLDER_WILL_SAVE_THE_MODEL \
--epochs NUMBER_OF_EPOCHS_USED_FOR_TRAINING \
--batch_size BATCH_SIZE_USED_FOR_TRAINING
```


# Testing

You can run simple validation by just running `./val.sh` or you can custom your validation by using a testing script.

## Testing Script

```bash
python3 validation.py \
--val_directory PATH_TO_FOLDER_STORE_VALIDATION_DATA \
--model_path PATH_TO_THE_MODEL_YOU_WANT_TO_VALIDATE
```