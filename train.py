import argparse
import os
import pandas as pd
from config import EPOCHS, BATCH_SIZE
from config import IMG_WIDTH, IMG_HEIGHT
from model import build_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.data import AUTOTUNE


def train(args):
    # Load data
    train_loader = image_dataset_from_directory(
        directory= args.train_directory,
        label_mode = 'categorical',
        seed= 123,
        image_size= (IMG_HEIGHT, IMG_WIDTH),
        batch_size= args.batch_size
    )
    train_dataset = train_loader.cache().shuffle(1000).prefetch(buffer_size= AUTOTUNE)

    val_dataset = None
    if args.val_directory != '':
        val_loader = image_dataset_from_directory(
            directory= args.val_directory,
            label_mode = 'categorical',
            seed= 123,
            image_size= (IMG_HEIGHT, IMG_WIDTH),
            batch_size= args.batch_size
        )
        val_dataset = val_loader.cache().prefetch(buffer_size= AUTOTUNE)

    # Build model
    model = build_model()

    model.compile(
        optimizer= 'adam',
        loss= 'categorical_crossentropy',
        metrics= ['accuracy']
    )

    best_model_path = os.path.join(args.model_directory, 'best_model.hdf5')
    if args.val_directory != '':
        call_backs = [
            ModelCheckpoint(best_model_path, monitor= 'val_accuracy', verbose= 1, save_best_only= True, mode= 'max')
        ]
    else:
        call_backs = [
            ModelCheckpoint(best_model_path, monitor= 'accuracy', verbose= 1, save_best_only= True, mode= 'max')
        ]
    
    # Training model
    training_history = model.fit(train_dataset, batch_size= args.batch_size, epochs= args.epochs, callbacks= call_backs, validation_data= val_dataset)
    
    history = pd.DataFrame(training_history.history)
    history_path = os.path.join(args.model_directory, 'history.csv')
    history.to_csv(history_path)

    final_model_path = os.path.join(args.model_directory, 'final_model.hdf5')
    model.save(final_model_path)


if __name__ == '__main__':
    # Parsing argument
    parser = argparse.ArgumentParser()
    
    parser.add_argument(
        '--train_directory',
        type= str,
        help= 'Path to the folder where store training data.',
        required= True
    )

    parser.add_argument(
        '--val_directory',
        type= str,
        help= 'Path to the folder where store validation data.',
        default= '',
        required= False
    )

    parser.add_argument(
        '--model_directory',
        type= str,
        help= 'Path to the folder where will store the result.',
        default= 'Model',
        required= False
    )

    parser.add_argument(
        '--epochs',
        type= int,
        help= 'Number of epochs used to train model.',
        default= EPOCHS,
        required= False
    )

    parser.add_argument(
        '--batch_size',
        type= int,
        help= 'Batch size used to train model.',
        default= BATCH_SIZE,
        required= False
    )

    args = parser.parse_args()

    # Training
    train(args)