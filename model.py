from tensorflow.keras.applications import Xception
from tensorflow.keras.layers import Dense, Input, Reshape, Permute, Add, Flatten, Dropout
from tensorflow.keras.layers.experimental.preprocessing import RandomFlip, RandomRotation, RandomZoom
from tensorflow.keras.models import Model, Sequential
from config import NUM_TOKEN, NUM_CHANNEL, NUM_BLOCK_MIXER
from config import N_CLASS, IMG_WIDTH, IMG_HEIGHT, IMG_CHANNEL


def build_model():
    # pretrained model
    base_model = Xception(
        include_top= False,
        weights= 'imagenet',
        input_tensor= None,
        input_shape= (256, 256, 3),
        pooling= 'avg',
        classes= 6,
        classifier_activation="softmax",
    )

    # data augment
    data_augmentation = Sequential(
        [
            RandomFlip("horizontal"),
            RandomRotation(0.1),
            RandomZoom(0.1),
        ]
    )

    # MLP Mixer
    token_mix = Sequential(
        [
            Permute((2, 1)),
            Dense(NUM_TOKEN),
            Permute((2, 1))
        ]
    )

    channel_mix = Sequential(
        [
            Dense(NUM_CHANNEL)
        ]
    )

    # build model
    model_input = Input(shape= (IMG_WIDTH, IMG_HEIGHT, IMG_CHANNEL))
    x = data_augmentation(model_input)
    x = base_model(x)
    x = Reshape((NUM_TOKEN, NUM_CHANNEL))(x)

    for i in range(NUM_BLOCK_MIXER):
        x = Add()([x, token_mix(x)])
        x = Add()([x, channel_mix(x)])
    
    x = Flatten()(x)
    x = Dropout(0.2)(x)
    x = Dense(256, activation= 'relu')(x)

    x = Dropout(0.2)(x)
    x = Dense(64, activation= 'relu')(x)

    x = Dropout(0.2)(x)
    model_output = Dense(N_CLASS, activation= 'softmax')(x)

    model = Model(inputs= model_input, outputs= model_output)
    return model


if __name__ == '__main__':
    model = build_model()
    model.summary()