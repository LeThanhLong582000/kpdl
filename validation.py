import argparse
from config import IMG_WIDTH, IMG_HEIGHT
from config import BATCH_SIZE
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.data import AUTOTUNE


def validation(args):
    # Load data
    val_loader = image_dataset_from_directory(
        directory= args.val_directory,
        label_mode = 'categorical',
        seed= 123,
        image_size= (IMG_HEIGHT, IMG_WIDTH),
        batch_size= BATCH_SIZE
    )
    val_dataset = val_loader.cache().prefetch(buffer_size= AUTOTUNE)

    # Load model
    model = load_model(args.model_path)

    # Validate model
    loss, accuracy = model.evaluate(val_dataset, batch_size= BATCH_SIZE)
    return loss, accuracy


if __name__ == '__main__':
    # Parsing argument
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--val_directory',
        type= str,
        help= 'Path to the folder where store validation data.',
        required= True
    )

    parser.add_argument(
        '--model_path',
        type= str,
        help= '',
        default= 'Model/final_model.hdf5',
        required= False
    )

    args = parser.parse_args()

    # Validation
    loss, accuracy = validation(args)
    print("Loss: {0:.4f}".format(loss))
    print("Accuracy: {0:.2f}%".format(accuracy * 100))